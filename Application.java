public class Application{
	public static void main(String[]args){
		//Step 4
		Dog d1=new Dog();
		d1.name="Bocchi";
		d1.age=1;
		d1.sex='f';
		d1.breed="Corgi";
		
		Dog d2=new Dog();
		d2.name="DongDong";
		d2.age=3;
		d2.sex='m';
		d2.breed="mixed";
		
		//System.out.println("test dog info:  name:"+d1.name+"  age:"+d1.age+"  sex:"+d1.sex+"  breed:"+d1.breed);
		//System.out.println("test dog info:  name:"+d2.name+"  age:"+d2.age+"  sex:"+d2.sex+"  breed:"+d2.breed);
		
		//step 7
		//Dog.eat(d1.name);
		
		//step 8
		//d1.run();
		//d2.eat();
		
		//Step 13: Array
		Dog[] pack= new Dog[3];
		
		pack[0]=d1;
		pack[1]=d2;
		
		//screenshot #5
		System.out.println(pack[0].name);
		System.out.println(pack[1].name);
		
		//Screenshot #6
		//System.out.println(pack[2].name);
		
		//step 20:
		pack[2]=new Dog();
		//pack[2].name="John";
		
		//step 21
		System.out.println(pack[2].name);
	}
}